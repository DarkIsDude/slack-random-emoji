package cmd

import (
	"fmt"

	"github.com/rivo/tview"

	slack "gitlab.com/DarkIsDude/slack-random-emoji/services"
)

var application *tview.Application

func Start() {
	application = tview.NewApplication()
	flex := tview.NewFlex()
	application.SetRoot(flex, true)

	channels := tview.NewList().ShowSecondaryText(false)
	channels.SetBorder(true).SetTitle("Channels")

	messages := tview.NewList().ShowSecondaryText(false)
	messages.SetBorder(true).SetTitle("Messages")

	flex.AddItem(channels, 0, 1, false)
	flex.AddItem(messages, 0, 1, false)

	err := populateChannels(channels, messages)
	if err != nil {
		panic(err)
	}

	application.SetFocus(channels)

	if err := application.Run(); err != nil {
		panic(err)
	}
}

func populateChannels(channelsUI *tview.List, messagesUI *tview.List) error {
	channels, err := slack.ListChannel()
	if err != nil {
		return err
	}

	for _, channel := range channels {
		localChannel := channel
		channelsUI.AddItem(
			channel.Name,
			fmt.Sprintf("%s (%d)", channel.Purpose.Value, channel.Num_members),
			0,
			func() {
				populateMessages(localChannel, messagesUI)
			},
		)
	}

	return nil
}

func populateMessages(channel slack.Channel, messagesUI *tview.List) {
	application.SetFocus(messagesUI)
	messagesUI.Clear()

	messages, err := slack.ListMessages(channel)
	if err != nil {
		messagesUI.AddItem(
			fmt.Sprintf("%s", err),
			"",
			0,
			func() {},
		)

		return
	}

	for _, message := range messages {
		localMessage := message

		messagesUI.AddItem(
			message.Text,
			"",
			0,
			func() {
				err := slack.React(channel, localMessage)
				if err != nil {
					messagesUI.AddItem(
						fmt.Sprintf("%s", err),
						"",
						0,
						func() {},
					)

					return
				}

				application.Stop()
			},
		)
	}
}
