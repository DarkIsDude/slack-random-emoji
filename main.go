package main

import (
	"gitlab.com/DarkIsDude/slack-random-emoji/cmd"
	"github.com/joho/godotenv"
)

func main() {
	godotenv.Load()

	cmd.Start()
}
